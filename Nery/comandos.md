# Comandos de mayor importancia
# Nery Herrera - 201602870

## Git status

Este comando sirve para ver el estado actual de nuestra rama.

## Git add * 

Este comando agrega todos los cambios que hemos realizado, basicamente es el primer paso para un commit

## Git commit -m "MSG"

Este es el comando rapido de commit, en la parte de MSG escribimos nuestro mensaje de commit. Con este comando podemos escribit commits rapidos y completos.

## Git checkout

Con este comando podemos navegar entre ramas.

## Git init

Con este comando damos inicio a la rama.

