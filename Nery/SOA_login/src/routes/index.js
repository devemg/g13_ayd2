/*MYSQL CONNECTION */
const mysql = require('mysql');
const connection = mysql.createConnection({
    /*host: '35.238.193.241',
    user: 'root',
    password: 'example',
    database: 'database_deliveryfood'*/
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DB,
    insecureAuth: true
});


const { Router } = require('express');
const router = Router();
const cors = require('cors');
var bodyParser = require('body-parser');
var corsOptions = { origin: true, optionsSuccessStatus: 200 };
router.use(cors(corsOptions));
router.use(bodyParser.json({ limit: '100mb', extended: true }));
router.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))

router.get('/hola', 
    (req,res) => res.json
    (
        {msg: 'Holis'}
    )
);

// -------------------------------------------------------------------------------------
// AREA DE CONSULTASR MAGICAS
router.post("/root-queryquemadas", (request, response, next) => {
    connection.query('CONSULTA', (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });
});

// -------------------------------------------------------------------------------------
// AREA DE CONSULTASR MAGICAS
router.post("/root-query", (request, response, next) => {
    const CONSULTA = request.body.consulta
    connection.query(CONSULTA, (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });
});

/*******************************************************************************************************
 * GETERS
 * *****************************************************************************************************/

// -------------------------------------------------------------------------------------
// Obtener todos los usuarios en general
router.get("/get-alluser", (request, response, next) => {
    connection.query('select * from usuario u', (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });
});

// -------------------------------------------------------------------------------------
// Obtener todos los usuarios en general
router.get("/get-allrols", (request, response, next) => {
    connection.query('select * from rol r', (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });
});

/*******************************************************************************************************
 * Registro
 * *****************************************************************************************************/

router.post("/registrar", (req, response, next) => {

    let part1 = 'insert into usuario (nombre, correo, direccion, telefono, contrasenia, rol_id) values (\"';
    let part2 = ')'
    let consulta = part1 + req.body.nombre + '\",\"' + req.body.correo + '\",\"' + req.body.direccion + '\",\"' + req.body.telefono + '\",\"' + req.body.contrasenia + '\",' + req.body.rol_id + ')';
    console.log(consulta)
    
    connection.query(consulta, (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });

});
    
/*******************************************************************************************************
 * login
 * *****************************************************************************************************/

router.post("/login", (request, response, next) => {

    let consulta = 'select * from usuario u where u.correo = \"' + request.body.email + '\" and u.contrasenia = \"' + request.body.password + '\"'
    console.log(consulta)
    
    connection.query(consulta, (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });

});

router.post("/l", (request, response, next) => {

    console.log(request.body);

    /*connection.query(
        'select * from usurio u where u.correo = \'' + request.body.email + '\'and u.contrasenia = \'' + request.body.password + '\''
        , (error, rows) => {
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });*/
});

router.get("/get-productos", (request, response, next) => {
    connection.query('select * from rol p', (error, rows) => {
        // if (error) throw error;
        if (error) {
            console.log(error);
            return
        }
        response.json(rows);
    });
    // connection.end(error => { response.json(error) });
});





module.exports = router;
