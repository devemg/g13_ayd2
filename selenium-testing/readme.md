# Selenium

## Descripción de Prueba automátizada

## Configuración 
Para hacer pruebas desde su computadora, utilizar el driver-local ubicado en ```src/lib/driver-local.js```
Este archivo se importa en las funciones ```src/lib/funciones.js```(primera línea).

Al terminar sus pruebas locales, dejar importado el archivo ```src/lib/driver.js``` para que funcione en los contenedores.

## Ejecución
Para ejecutar las pruebas, utilizar el comando ```npm start```.
Con este comando se ejecutarán todas las pruebas que hay en la carpeta ```src/test```.

### Login Success 
Autenticación correcta: La prueba validará que ingresando el usuario 
y la contraseña correcta la aplicación lo redirigía hacia la página 
principal, en donde verificará que aparezca el nombre correcto del 
usuario que ingreso.

``` src/test/auth-success.js ```
    
Nota: Se agregó un id a los componentes utilizados en login.component.html para poder buscarlos.

#### Funciones 
Ubicadas en ```src/lib/funciones.js```
Estas funciones se encargan de buscar los elementos en le html de la página a la que se está haciendo la prueba.
Se pueden hacer operaciones sobre los elementos tales como obtener valores, asignar valores, etc.

Actualmente hay definidas las siguientes operaciones: 

- visit: Visitar página
- quit: Salir de la página
- findById: Buscar por id
- findByName: Buscar por nombre
- write : Escribir texto

Si necesitan más funciones parecidas, se agregan en los dos archivos de dirver.

```
/**
 * Buscar componentes de login
 * @param {*} id 
 */
Page.prototype.findLoginComponents = async function () { 
    //buscar input de correo
    loginInput = await this.findById("login-email");

    //buscar input de contraseña
    passwdInput = await this.findById("login-password");

    // buscar boton de ingresar
    loginButton = await this.findById("login-btn");

    // obtener texto del boton
    const btnValue = await loginButton.getText();

    //obtiene el tipo del input de contraseña
    const type = await passwdInput.getAttribute('type')

    const result = await this.driver.wait(async function() {
        // verificar si los input están habilitados
        const searchInputEnableFlag = await loginInput.isEnabled();
        const searchInputEnableFlag2 = await passwdInput.isEnabled();
        return {
            inputEmailEnabled: searchInputEnableFlag,
            inputPasswordEnabled: searchInputEnableFlag2,
            intputPasswordType: type,
           buttonText: btnValue
        }
    }, 5000);
    return result;
 }

 /**
  * Submit Login
  * @returns 
  */
 Page.prototype.submitLogin = async function() {
     //busca los componentes
    await this.findLoginComponents();
    
    // si necesitan valores random, se agrega el tipo de dato a generar en utils/fakeData.js 
    // y luego se importa. Pueden consultar el ejemplo de google-test

    //escribe un valor en el input del login
    await this.write(loginInput, 'garciam.emm@gmail.com');

    // escribe un valor en el input de contraseña
    await this.write(passwdInput, '1234');

    //hace click en el boton
    await loginButton.click();

    return await this.driver.wait(async function() {
        return 'ok';
    },5000);
};
```

#### Auth Success
 Ubicado en ```src/test/auth-success.js```
Se encarga de realizar llamar a las funciones y evaluar las respuestas por medio de pruebas
```
(async function example() {
    try {

        describe('Testeo para inicio de sesión exitoso', async function() { 
            this.timeout(50000);
            var driver, page;

            // se ejecuta antes de las pruebas (its)
            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                //visita la página
                // pendiente de cambiar url
                await page.visit('http://localhost:4200/iniciar-sesion');
            });

            // se ejecuta después de las pruebas (its)
            afterEach(async() => {
                //salir de la pagina
                await page.quit();
            });


            it('Buscar las cajas de texto y boton', async() => {  
                // busca los componentes              
                const result = await page.findLoginComponents();

                //se espera que el input de correo esté habilitado
                expect(result.inputEmailEnabled).to.equal(true);

                // se espera que el input de la contraseña esté habilitado
                expect(result.inputPasswordEnabled).to.equal(true);

                // se espera que el input de contraseña sea tipo password
                expect(result.intputPasswordType).to.equal('password');

                // se espera que el boton contenga la palabra ingresar
                expect(result.buttonText.toLowerCase()).to.include('ingresar');
                
            });

            it('Ingresar información y acceder', async() => {
                await page.submitLogin();
                //expect(result.length).to.be.above(10);
            });

            //redireccionar a catalogo y comprobar el nombre del usuario

        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();

```
