const faker = require('faker');

module.exports = {
    nameKeyword: faker.name.findName(),
    randomEmail: faker.internet.email(),
    randomPassword: faker.internet.password(),
    randomImage: faker.image.food()
};
