const { describe, it, after, before } = require('mocha');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const URL = 'http://34.70.53.40'

const Page = require('../lib/funciones');
process.on('unhandledRejection', () => {});

(async function AuthFail(){

    try{
        describe('Testeo para el login fallido', async function(){
            this.timeout(50000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit(`${URL}/iniciar-sesion`);
            });

            afterEach(async() =>{
                await page.quit();
            });

            it('Buscar los elementos de input correo, input password y button ingresar', async() =>{
                const result = await page.findInputAndButton();
                expect(result.userEnabled).to.equal(true);
                expect(result.passwordEnabled).to.equal(true);
                expect(result.buttonText.toLowerCase()).to.include('ingresar');
            });

            it('Mostrar el mensaje de autenticacion fallida', async() => {
                const result = await page.sendDataAndGetResult();
                expect(result).to.equal('Datos incorrectos');
            });

        });
    }catch(ex){
        console.log(new Error(ex.message));
    }finally{

    }

})();