const { describe, it, after, before } = require('mocha');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const URL = 'http://34.70.53.40'

const Page = require('../lib/funciones');
process.on('unhandledRejection', () => {});

(async function AuthFail(){

    try{
        describe('Testeo para creacion de producto fallido', async function(){
            this.timeout(50000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit(`${URL}/productos/nuevo`);
            });

            afterEach(async() =>{
                await page.quit();
            });

            it('Buscar las cajas de texto', async() =>{
                const result = await page.findInputAndButtonProducto();
                expect(result.nombreEnabled).to.equal(true);
                expect(result.precioEnabled).to.equal(true);
                expect(result.ofertaEnabled).to.equal(true);
                expect(result.descripcionEnabled).to.equal(true);
                expect(result.imageEnabled).to.equal(true);
                expect(result.buttonText.toLowerCase()).to.include('prueba');
            });

            it('LLenar los campos y obtener respuesta', async() => {
                const result = await page.sendDataProducto();
                expect(result).to.equal('producto no guardado');
            });

        });
    }catch(ex){
        console.log(new Error(ex.message));
    }finally{

    }

})();