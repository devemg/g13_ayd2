const { describe, it } = require('mocha');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const URL = 'http://34.70.53.40'

const Page = require('../lib/funciones');
process.on('unhandledRejection', () => {});

(async function example() {
    try {

        describe('Testeo para inicio de sesión exitoso', async function() { 
            this.timeout(50000);
            var driver, page;


            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit(`${URL}/iniciar-sesion`);
            });

            afterEach(async() => {
                await page.quit();
            });


            it('Buscar las cajas de texto y boton', async() => {                
                const result = await page.findLoginComponents();
                expect(result.inputEmailEnabled).to.equal(true);
                expect(result.inputPasswordEnabled).to.equal(true);
                expect(result.intputPasswordType).to.equal('password');
                expect(result.buttonText.toLowerCase()).to.include('ingresar');
            });

            it('Ingresar información y acceder', async() => {
                let result = await page.submitLogin();
               // expect(result).to.be.equal('Datos correctos');
                //check
                //await page.visit(`${URL}/catalogo`);

                let result2 = await page.checkLogin();
                expect(result2.username).to.be.equal(true);
            });

        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
