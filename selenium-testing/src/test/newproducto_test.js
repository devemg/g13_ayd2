const { describe, it } = require('mocha');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const URL = 'http://34.70.53.40'

const Page = require('../lib/funciones');
process.on('unhandledRejection', () => {});

(async function example() {
    try {

        describe('Testeo para new producto', async function() { 
            this.timeout(50000);
            var driver, page;


            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit(`${URL}/productos/nuevo`);
            });

            afterEach(async() => {
                await page.quit();
            });


            
            it('Buscar las inputs', async() => {                
                const result = await page.findNewProductComponents();
                expect(result.inputEmailEnabled).to.equal(true);
                expect(result.inputPasswordEnabled).to.equal(true);
            });

            it('Ingresar producto', async() => {
                let result = await page.submitNewProduct();
                expect(result).to.be.equal('OK');
            });

        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
