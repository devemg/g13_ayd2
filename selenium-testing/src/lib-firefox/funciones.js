let Page = require('./driver');

const fake = require('../utils/fakeData');
const fakeNameKeyword = fake.nameKeyword;
const fakeEmail = fake.randomEmail;
const fakePassword = fake.randomPassword;

let searchInput, searchButton, resultStat;

let nombreInput, precioInput, ofertaInput;
let descripcionInput, imageInput, buttonGuardar;
let userInput, passwordInput, ingresarButton, resultMessage;
let loginInput,passwdInput,loginButton;
let Resultmensaje;

Page.prototype.findInputAndButton = async function() {
    searchInput = await this.findByName("q");
    searchButton = await this.findByName("btnK");

    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag = await searchInput.isEnabled();
        const searchButtonText = await searchButton.getAttribute('value');
        return {
            inputEnabled: searchInputEnableFlag,
            buttonText: searchButtonText
        }
    }, 5000);
    return result;
};


Page.prototype.submitKeywordAndGetResult = async function() {
    await this.findInputAndButton();
    await this.write(searchInput, fakeNameKeyword);
    await searchInput.sendKeys("\n");

    resultStat = await this.findById("result-stats");
    return await this.driver.wait(async function() {
        return await resultStat.getText();
    },5000);
};

/**
 * Buscar componentes de login
 * @param {*} id 
 */
Page.prototype.findLoginComponents = async function () { 
    loginInput = await this.findById("login-email");
    passwdInput = await this.findById("login-password");
    loginButton = await this.findById("login-btn");

    const btnValue = await loginButton.getText();
    const type = await passwdInput.getAttribute('type');
    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag = await loginInput.isEnabled();
        const searchInputEnableFlag2 = await passwdInput.isEnabled();
        return {
            inputEmailEnabled: searchInputEnableFlag,
            inputPasswordEnabled: searchInputEnableFlag2,
            intputPasswordType: type,
            buttonText: btnValue
        }
    }, 5000);
    return result;
 }

 /**
  * Submit Login
  * @returns 
  */
 Page.prototype.submitLogin = async function() {
    await this.findLoginComponents();
    await this.write(loginInput, 'selenium');
    await this.write(passwdInput, 'selenium');
    await loginButton.click();
    resultMessage = await this.findById("message");

    return await this.driver.wait(async function(){
        return await resultMessage;
    }, 5000);
};

 /**
  * Search components login success
  * buscar opcion de iniciar sesion
  * buscar opcion de registrarse
  * buscar nombre del usuario
  * @returns 
  */
  Page.prototype.checkLogin = async function() {
    loginButton = await this.findById("menu-user");
    return await this.driver.wait(async function() {
        return {
            username: loginButton != null
        };
    },5000);
};


/**
 * Buscar componentes de login
 * @param {*} id 
 */
 Page.prototype.findNewProductComponents = async function () { 
    input_newproduct_nombre = await this.findById("newproduct-nombre");
    input_newproduct_precio = await this.findById("newproduct-precio");
    NewProductoButton = await this.findById("btn_guardar");
    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag = await input_newproduct_nombre.isEnabled();
        const searchInputEnableFlag2 = await input_newproduct_precio.isEnabled();
        return {
            inputEmailEnabled: searchInputEnableFlag,
            inputPasswordEnabled: searchInputEnableFlag2
        }
    }, 5000);
    return result;
 }

  /**
  * Submit Login
  * @returns 
  */
   Page.prototype.submitNewProduct = async function() {
    await this.findNewProductComponents();
    await this.write(input_newproduct_nombre, 'ProductoPrueba');
    await this.write(input_newproduct_precio, '100');
    await NewProductoButton.click();
    return await this.driver.wait(async function() {
        return 'OK';
    },5000);
};


Page.prototype.findInputAndButtonProducto = async function(){
    nombreInput = await this.findByName("nombre");
    precioInput = await this.findByName("precio");
    ofertaInput = await this.findByName("oferta");
    descripcionInput = await this.findByName("descripcion");
    imageInput = await this.findByName("imagen");
    await this.visible();
    buttonGuardar = await this.findByName("btn_prueba");

    const result = await this.driver.wait(async function(){
        const nombreFlag = await nombreInput.isEnabled();
        const precioFlag = await precioInput.isEnabled();
        const ofertaFlag = await ofertaInput.isEnabled();
        const descripcionFlag = await descripcionInput.isEnabled();
        const imageFlag = await imageInput.isEnabled();
        const buttonGuardarText = await buttonGuardar.getAttribute('value');

        return{
            nombreEnabled: nombreFlag,
            precioEnabled: precioFlag,
            ofertaEnabled: ofertaFlag,
            descripcionEnabled: descripcionFlag,
            imageEnabled: imageFlag,
            buttonText: buttonGuardarText
        }

    }, 50000);
    return result;
};


Page.prototype.sendDataProducto = async function(){
    await this.findInputAndButtonProducto();
    await this.write(nombreInput, "");
    await this.write(precioInput, 10);
    await this.write(ofertaInput, 10);
    await this.write(descripcionInput, "Exquisito pollo frito");
    await buttonGuardar.click();

    Resultmensaje = await this.findById("mensaje");

    return await this.driver.wait(async function(){
        return await Resultmensaje.getText();
    },50000);


};

/**
 * mando a verificar la existencia de los elementos
 */

 Page.prototype.findInputAndButton = async function(){

    userInput = await this.findByName("user");
    passwordInput = await this.findByName("password");
    ingresarButton = await this.findByName("ingresar");

    const result = await this.driver.wait(async function(){

        const userInputFlag = await userInput.isEnabled();
        const passwordInputFlag = await passwordInput.isEnabled();
        const ingresarButtonText = await ingresarButton.getAttribute('value');

        return{
            userEnabled: userInputFlag,
            passwordEnabled: passwordInputFlag,
            buttonText: ingresarButtonText
        }

    }, 50000);
    return result;
};


Page.prototype.sendDataAndGetResult = async function(){

    await this.findInputAndButton();
    await this.write(userInput, fakeEmail);
    await this.write(passwordInput, fakePassword);
    await ingresarButton.click();

    resultMessage = await this.findById("message");

    return await this.driver.wait(async function(){
        return await resultMessage.getText();
    }, 50000);

};

module.exports = Page;
