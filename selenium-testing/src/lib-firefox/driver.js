const { Builder, By, until, Key } = require('selenium-webdriver');

SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
//var cbtHub = `http://${process.env.HUB_HOST}:4444/wd/hub`

var cbtHub = `http://34.68.188.204:4444/wd/hub`

var caps = {
    name: 'FireFox Test',
    setPageLoadStrategy: 'eager',
    browserName: 'firefox',
    browserVersion: '71.0'
};



var Page = function() {

    this.driver = new Builder()
    //.setChromeOptions(options)
    .withCapabilities(caps)
    .usingServer(cbtHub)
    //.forBrowser('chrome')
    .build();


    // visit a webpage
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };


    // quit current session
    this.quit = async function() {
        return await this.driver.quit();
    };


 // wait and find a specific element with it's id
 this.findById = async function(id) {
    await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
    return await this.driver.findElement(By.id(id));
};

 // wait and find a specific element with it's name
 this.findByName = async function(name) {
    await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
    return await this.driver.findElement(By.name(name));
};


 // fill input web elements
this.write = async function(el, txt) {
    return await el.sendKeys(txt);
};


this.visible = async function(){
    return await this.driver.executeScript("document.getElementById('prueba').style.display='block';")
}


};

module.exports = Page;

