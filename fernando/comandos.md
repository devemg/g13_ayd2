
# Comandos Importantes GIT

## git init 

Permite inicializar el repositorio de git.

## git commit

Permite realizar un commit de los cambios previamente agregados a stage agregando una descripcióón para que otros desarrolladores entiendan los cambios.

## Git branch
Nos muestra una lista de las branches que existen en el repositorio.


## Git add . 
Git add nos permite agregar a los archivos editados o creados para que estos se puedan agregar a un commit.

## Git status
El comando git status nos deja ver los archivos el estado actual de los archivos si estos ya estan agregados se marcan de color verde y si no estan agregados se marcan en rojo.

