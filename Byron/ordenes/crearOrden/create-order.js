const express = require('express');
const { request, response } = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const app = express();

//configuraciones
app.use(bodyparser.json());
app.use(cors());


//conexión
app.set('port', process.env.PORT || 8080);
var mysql = require('mysql');
var con = mysql.createConnection({
  //host: "35.238.193.241",
  host:"34.70.53.40",
  user: "root",
  password: "example",
  database:"database_deliveryfood"
  });
  
  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
     

        
      });
      app.listen(3000, () => {
        console.log("MICROSERVICE: Create product running on port 8080");
    });
    
  // obtener los distintos estado de una orden
app.get('/get-productos', (request, response, next) =>{
  var sql = `
  Select * from 
  producto;
  `;

  con.query(sql, (err, result)=>{
    if(err){
      console.error(err);
      return response.json({ 'error': "Error al obtener", "status": 500 });
    }
    return response.json({"datos": result, "status":200});
});
});

// obtener la lista de ordenes del cliente logueado
app.get('/get-orden-cliente', (request, response, next) =>{
  var id_usuario = request.query['id'];

  var sql = 
  `
    SELECT od.fecha_hora, od.total,
    usr.direccion,
    st_od.nombre as nombre_estado
    FROM
    orden od
    INNER JOIN
    usuario usr
    ON od.cliente_id = usr.id
    INNER JOIN
    estado_orden st_od
    ON od.estado_id = st_od.id
    where usr.id = ?;
  `;
  con.query(sql, [id_usuario], (err, result)=>{
    if(err){
      
      return response.json({ 'error': "Error al insertar", "status": 500 });
    }
    //LA ETIQUETA DATOS CREO QUE LA TENGO QUE CAMBIAR PORQUE NO ME SIRVE DE NADA
    return response.json({"datos":result, "status": 200});
  })
})
app.get('/get-orden', (request, response, next) =>{
  var sql = `
  Select * from 
  orden;
  `;

  con.query(sql, (err, result)=>{
    if(err){
      console.error(err);
      return response.json({ 'error': "Error al obtener", "status": 500 });
    }
    return response.json({"datos": result, "status":200});
});
});
app.get('/get-usuarios', (request, response, next) =>{
  var sql = `
  Select * from 
  usuario;
  `;

  con.query(sql, (err, result)=>{
    if(err){
      console.error(err);
      return response.json({ 'error': "Error al obtener", "status": 500 });
    }
    return response.json({"datos": result, "status":200});
});
});

app.get('/get-restaurante', (request, response, next) =>{
  var sql = `
  Select * from 
  restaurante;
  `;

  con.query(sql, (err, result)=>{
    if(err){
      console.error(err);
      return response.json({ 'error': "Error al obtener", "status": 500 });
    }
    return response.json({"datos": result, "status":200});
});
});

app.post("/create-producto", (request, response, next) => {
  var { nombre,foto,precio,descripcion,oferta,restaurante } = request.body;
  var sql = `INSERT INTO producto ( nombre,fotografia,precio,descripcion,oferta,restaurante)
              VALUES (?,?,?,?,?,?)`;
  var values = [nombre,foto,precio,descripcion,oferta,restaurante];
  con.query(sql, values, function(err, result){
      if (err){
          console.log(err);
          return response.json({ 'error': "Error al insertar en la tabla ORDEN", "code": err });
          
      }
      return response.json(result); 

});

});



   
  app.post("/create-order", (request, response, next) => {
    var { total,clienteid,usuarioid,estadoid } = request.body;
    var sql = `INSERT INTO orden (total,cliente_id, usuario_id, estado_id)
                VALUES (?,?,?,?)`;
    var values = [total,clienteid,usuarioid,estadoid];
    con.query(sql, values, function(err, result){
        if (err){
            console.log(err);
            return response.json({ 'error': "Error al insertar en la tabla ORDEN", "code": err });   
        }
        return response.json(result); 

  });
  
});

app.post("/fill-order", (request, response, next) => {
  var {cantidad, idproducto, idorder} = request.body;
  var sql = `INSERT INTO orden_producto (cantidad, producto_id, orden_id)
              VALUES (?,?,?)`;
  var values = [cantidad,idproducto,idorder];
  con.query(sql, values, function(err, result){
      if (err){
          console.log(err);
          return response.json({ 'error': "Error al insertar en la tabla ORDEN_PRODUCTO", "code": err });
      }
      return response.json(result); 

    });

});

 
// esto trae la informacion de ordenes, para mostrarlas en el crud de restaurante
app.get('/get-orden-detalle', (request, respose, next) =>{
  var sql = 
  `SELECT od.id,od.fecha_hora, od.total, 
  usr.nombre as nombre_cliente, usr.direccion,
  st_od.id as estado_orden,st_od.nombre as nombre_estado
  FROM
  orden od
  INNER JOIN
  usuario usr
  ON od.cliente_id = usr.id
  INNER JOIN
  estado_orden st_od
  ON od.estado_id = st_od.id;`;

  con.query(sql, (err, result)=>{
    if(err){
      console.log(err);
      return response.json({ 'error': "Error al insertar", "status": 500 });
    }
    return respose.json(result);

  })

});

// obtener los distintos estado de una orden
app.get('/get-estado', (request, response, next) =>{
  var sql = `
  Select * from 
  estado_orden;
  `;

  con.query(sql, (err, result)=>{
    if(err){
      console.error(err);
      return response.json({ 'error': "Error al obtener", "status": 500 });
    }
    return response.json({"datos": result, "status":200});
});





 

});

app.put('/update/estado-orden', (request, response, next) =>{

  var id_estado = request.param('id_estado');
  var id_orden = request.param('id_orden');

  //console.log(id_estado, id_orden);

  var sql = `
    UPDATE orden
    set estado_id = ? 
    WHERE id = ?
  `;
  con.query(sql, [id_estado,id_orden], (err, result) =>{
    if (err) {
      return response.json({ 'error': "Error al insertar", "status": 500 });
  };
  return response.json({ 'msg': result, 'status': 200 });

  });

});




