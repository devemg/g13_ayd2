# Comandos Git

## Git help
Muestra la lista de comandos mas utilizados en Git.

## Git init
Se utiliza para crear un repositorio local, creando el branch master por defecto en la carpeta que estemos localizados.

## Git add -A 
Agrega al repositorio todos los archivos y/o carpetas de nuestro proyecto.

## Git add + path
Agrega todo el contenido de la dirección o path que indiquemos.

## Git branch
Nos muestra una lista de las branches que existen en el repositori.



