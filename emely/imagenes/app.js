
var bodyParser = require("body-parser");
var express = require("express");
var app = express();
var multipart = require('connect-multiparty');
var cors = require('cors')
var multipartMiddleware = multipart();

var cloudinary = require('cloudinary').v2;
cloudinary.config({ 
  cloud_name: process.env.CLOUD_NAME || 'devemg', 
  api_key: process.env.API_KEY || '581653394872287', 
  api_secret: process.env.API_SECRET || '1aYcuT58xhtFKiAjW2qqjrb1OEw' 
});

//JSON
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}));
// cors
app.use(cors())

function createPhoto(req,res){

  // Get temp file path
  var imageFile = req.files.image.path;
  // Upload file to Cloudinary
  cloudinary.uploader.upload(imageFile, { tags: 'ayd2_proyecto1' })
    .then(function (image) {
      return image;
    })
    .then(function (img) {
      return res.status(200).send({
        url: img.url
      });
    })
    .catch(e=>{
      return res.status(400).send({
        msg:'Error al subir la imagen'
      });
    });
}

/**
 * POST Image
 * save in AWS S3 
 * return url
 */
app.post("/upload-image",multipartMiddleware, createPhoto);


app.listen(8080, () => {
    console.log("MS: Upload Images on port 8080");
});