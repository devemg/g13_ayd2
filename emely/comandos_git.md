
# Comandos Importantes GIT

## git init 

Permite inicializar el repositorio de git.

## git commit

Permite realizar un commit de los cambios previamente agregados a stage agregando una descripcióón para que otros desarrolladores entiendan los cambios.

## git push

Permite subir los cambios locales en la rama remota.

## git pull

Permite sincronizar los cambios locales con los cambios en la rama remota.

## git logs

Permite ver un historial de los commits realizados.
