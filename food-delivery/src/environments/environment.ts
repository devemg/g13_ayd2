// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  name: 'dev',
  BACKIP: '34.70.53.40'
  
};

/**
 * IP de servidor de testing en gc
 */
 export const API_PRODS = {
   GET_PRODUCTS:`http://${environment.BACKIP}:3000/get-productos`,
   SINGLE_PRODUCT:`http://${environment.BACKIP}:3000/get/product/`,
   CREATE_PRODUCT:`http://${environment.BACKIP}:1000/create-product`,
   UPDATE_PRODUCT:`http://${environment.BACKIP}:4000/update/product/`,
   DELETE_PRODUCT:`http://${environment.BACKIP}:2000/delete/product/`,
   OFFER_PRODUCT:`http://${environment.BACKIP}:3000/offer/get-productos`,
   UPLOAD_IMAGES: `http://${environment.BACKIP}:8080/upload-image`,
   CATALOGO_GETPRODUCTOS: `http://${environment.BACKIP}:3000/get-productos`,
   CATALOGO_SEARCHPRODUCTOS: `http://${environment.BACKIP}:3000/search`,
   API_USUARIOS: `http://${environment.BACKIP}:3666/`,
   GET_RESTAURANTS: `http://${environment.BACKIP}:3000/get-restaurantes`,
   CREATE_RESTAURANT: `http://${environment.BACKIP}:1000/create-restaurant`,
   UPDATE_RESTAURANTS: `http://${environment.BACKIP}:4000/update/restaurant/`,
   SINGLE_RESTAURANT: `http://${environment.BACKIP}:3000/get/restaurant/`,
   DELETE_RESTAURANT: `http://${environment.BACKIP}:2000/delete/restaurant/`,
   SEARCH_RESTAURANT: `http://${environment.BACKIP}:3000/search/restaurant/?q=monitor`,
   PRODUCTS_OF_RESTAURANT: `http://${environment.BACKIP}:3000/get/restaurant/products/?id=2`
   
 }
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
