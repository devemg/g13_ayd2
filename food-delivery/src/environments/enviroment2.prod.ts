export const environment = {
    production: true,
    name: 'production2',
    BACKIP: '35.227.74.31'
  };
  
  /**
   * IP de produccion 2
  */ 
  export const API_PRODS = {
    GET_PRODUCTS:`http://${environment.BACKIP}:3000/get-productos`,
    SINGLE_PRODUCT:`http://${environment.BACKIP}:3000/get/product/`,
    CREATE_PRODUCT:`http://${environment.BACKIP}:1000/create-product`,
    UPDATE_PRODUCT:`http://${environment.BACKIP}:4000/update/product/`,
    DELETE_PRODUCT:`http://${environment.BACKIP}:2000/delete/product/`,
    OFFER_PRODUCT:`http://${environment.BACKIP}:3000/offer/get-productos`,
    UPLOAD_IMAGES: `http://${environment.BACKIP}:8080/upload-image`,
    CATALOGO_GETPRODUCTOS: `http://${environment.BACKIP}:3000/get-productos`,
    CATALOGO_SEARCHPRODUCTOS: `http://${environment.BACKIP}:3000/search`,
    API_USUARIOS: `http://${environment.BACKIP}:3666/`,
    GET_RESTAURANTS: `http://${environment.BACKIP}:3000/get-restaurantes`,
    CREATE_RESTAURANT: `http://${environment.BACKIP}:1000/create-restaurant`,
    UPDATE_RESTAURANTS: `http://${environment.BACKIP}:4000/update/restaurant/`,
    SINGLE_RESTAURANT: `http://${environment.BACKIP}:3000/get/restaurant/`,
    DELETE_RESTAURANT: `http://${environment.BACKIP}:2000/delete/restaurant/`,
    SEARCH_RESTAURANT: `http://${environment.BACKIP}:3000/search/restaurant/?q=monitor`,
    PRODUCTS_OF_RESTAURANT: `http://${environment.BACKIP}:3000/get/restaurant/products/?id=2`
  }
  