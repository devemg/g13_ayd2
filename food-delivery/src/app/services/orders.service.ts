import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Orden_detalle, Estado_Orden, Orden_cliente, Res_Orden, Res_Estado} from '../models/orden.interface';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  //TODO: cambiar la direccion ip o agregar variable de entorno
  API_URI ="http://"+ environment.BACKIP + ":3010";

  constructor(private http: HttpClient) { }

  get_orden(): Observable<Array<Orden_detalle>>{
    return this.http.get<Array<Orden_detalle>>(`${this.API_URI}/get-orden-detalle`);
  }

  get_estado(): Observable<Res_Estado>{
    return this.http.get<Res_Estado>(`${this.API_URI}/get-estado`)
  }

  update_orden(datos:any){
    return this.http.put(`${this.API_URI}/update/estado-orden`,datos);
  }

  get_orden_cliente(id:number):Observable<Res_Orden>{
    return this.http.get<Res_Orden>(`${this.API_URI}/get-orden-cliente/?id=${id}`);
  }
 
  crearOrden(datos:any):Observable<any>{
    return this.http.post<any>(`${this.API_URI}/create-order`,datos);
  }

  getOrdenes(){
    return this.http.get(`${this.API_URI}/get-orden`)
  }
  fillOrder(datos:any):Observable<any>{
    return this.http.post<any>(`${this.API_URI}/fill-order`,datos)
  }
}
