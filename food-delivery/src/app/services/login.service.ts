import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' 
import { tap,map } from 'rxjs/operators';
import { Observable, BehaviorSubject, from, Subject } from 'rxjs';
import { FormatWidth, formatDate } from '@angular/common';
import { API_PRODS } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService { 

  isLogged = localStorage.getItem('usuarioactual')!=null; 
  
  isLogged$: Subject<boolean>;

  constructor(private http: HttpClient) {
    this.isLogged$ = new Subject();
   }

  
  getAllUser()
  {
    return this.http.get(API_PRODS.API_USUARIOS + 'get-alluser');
  }

  loginUser(usuario: any)
  {
    return this.http.post(API_PRODS.API_USUARIOS + 'login', usuario);
  }

  registrarUser(usuario: any)
  {
    return this.http.post(API_PRODS.API_USUARIOS + 'registrar', usuario);
  }

  root_mysql(usuario: any)
  {
    return this.http.post(API_PRODS.API_USUARIOS + 'root-query', usuario);
  }

  setIsLogged(value: boolean){
    this.isLogged = value; 
    this.isLogged$.next(this.isLogged);
  }

  isLoggedVar(){
    return this.isLogged$.asObservable();
  }

}
