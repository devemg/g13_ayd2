import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Catalogo, Producto} from '../../models/productos.interface';
import { Observable } from 'rxjs';
import { API_PRODS } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogoProductoService {

  constructor(private http: HttpClient) { }

  get_productos(): Observable<Catalogo>{
    return this.http.get<Catalogo>(`${API_PRODS.CATALOGO_GETPRODUCTOS}`);
  }

  search_api(nombre: string): Observable<Catalogo>{
    return this.http.get<Catalogo>(`${API_PRODS.CATALOGO_SEARCHPRODUCTOS}/?q=`+nombre.toString());
  }

  getDummyProds(): Catalogo{
    return {
      msg:[
        {
          nombre:'Producto A',
          fotografia:'https://www.unileverfoodsolutions.com.mx/dam/global-ufs/mcos/NOLA/calcmenu/recipes/MX-recipes/general/hamburguesa-cl%C3%A1sica/main-header.jpg',
          precio:15,
          restaurante:'Restaurante A',
          descripcion:'A switch has the markup of a custom checkbox but uses the .custom-switch class to render a toggle switch. Switches also support the disabled attribute.'
        },
        {
          nombre:'Producto A',
          fotografia:'https://www.unileverfoodsolutions.com.mx/dam/global-ufs/mcos/NOLA/calcmenu/recipes/MX-recipes/general/hamburguesa-cl%C3%A1sica/main-header.jpg',
          precio:15,
          restaurante:'Restaurante A',
          descripcion:'A switch has the markup of a custom checkbox but uses the .custom-switch class to render a toggle switch. Switches also support the disabled attribute.'
        },
        {
          nombre:'Producto A',
          fotografia:'https://www.unileverfoodsolutions.com.mx/dam/global-ufs/mcos/NOLA/calcmenu/recipes/MX-recipes/general/hamburguesa-cl%C3%A1sica/main-header.jpg',
          restaurante:'Restaurante A',
          precio:15,
          oferta:12,
          descripcion:'A switch has the markup of a custom checkbox but uses the .custom-switch class to render a toggle switch. Switches also support the disabled attribute.'
        },
        {
          nombre:'Producto A',
          fotografia:'https://www.unileverfoodsolutions.com.mx/dam/global-ufs/mcos/NOLA/calcmenu/recipes/MX-recipes/general/hamburguesa-cl%C3%A1sica/main-header.jpg',
          restaurante:'Restaurante A',
          precio:15,
          descripcion:'A switch has the markup of a custom checkbox but uses the .custom-switch class to render a toggle switch. Switches also support the disabled attribute.'
        },
        {
          nombre:'Producto A',
          fotografia:'https://www.unileverfoodsolutions.com.mx/dam/global-ufs/mcos/NOLA/calcmenu/recipes/MX-recipes/general/hamburguesa-cl%C3%A1sica/main-header.jpg',
          restaurante:'Restaurante A',
          precio:15,
          descripcion:'A switch has the markup of a custom checkbox but uses the .custom-switch class to render a toggle switch. Switches also support the disabled attribute.'
        }
      ]
    }
  }
  
}

