import { Injectable } from '@angular/core';

export interface Producto {
  id:number; 
  nombre:string;
  cantidad:number; 
  imagen:string; 
  precio:number;
}

@Injectable({
  providedIn: 'root'
})
export class CarritoComprasService {

  constructor() { 
    // this.saveCurrentProductos()
  }

  getProductos(){
    var productos:Producto[] = [];
    let prod:any = localStorage.getItem('productos');
    productos = prod?JSON.parse(prod):[]
    return productos;
  }

  addProducto(producto:Producto){
    var productos = this.getProductos()
    let index = productos.findIndex(value=>value.id == producto.id);    
    if(index >= 0){
      productos[index].cantidad = productos[index].cantidad + producto.cantidad; 
    }else{
      productos.push(producto);
    }
    this.saveProductos(productos);
  }

  deleteProducto(id:number){
    var productos = this.getProductos()  
    let index = productos.findIndex(value=>value.id == id);    
    if(index >= 0){
      productos.splice(index,1)
      this.saveProductos(productos)
    }
  
  }

  saveProductos(productos:Producto[]){
    localStorage.setItem('productos',JSON.stringify(productos))
  }

  getTotal(){
    let total = 0;
    var productos = this.getProductos(); 
    productos.forEach(res=>{
      total += (res.cantidad * res.precio)
    })
    return total;
  }

  clearCarrito(){
    this.saveCurrentProductos();
  }

  private saveCurrentProductos(){
    let producto:Producto[] = [];
    localStorage.setItem('productos',JSON.stringify(producto))
  }


}
