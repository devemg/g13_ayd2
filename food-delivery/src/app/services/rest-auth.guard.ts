import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestAuthGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    if(sesion){
      let usuariologeado = sesion[0];
      console.log(usuariologeado)
      if(usuariologeado){
        if(usuariologeado.rol_id == 2){
          return true;
        }
      }
    }
    this.router.navigateByUrl('');
    return false;
  }
  
}
