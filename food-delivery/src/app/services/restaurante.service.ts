import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_PRODS } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestauranteService {

  constructor(private http: HttpClient) { }

   /**
   * Obtener restauranteos
   */
    getrestaurantes():Promise<any[]>{
      return this.http.get<any[]>(API_PRODS.GET_RESTAURANTS).toPromise();
    }
  
    /**
     * Obtener restauranteo
     * @param id 
     */
    getrestaurante(id:number):Promise<any[]>{
      return this.http.get<any[]>(`${API_PRODS.SINGLE_RESTAURANT}?id=${id}`).toPromise();
    }
  
    /**
     * Agregar restauranteo
     * @param restaurante 
     */
    addrestaurante(restaurante:any){
      return this.http.post(API_PRODS.CREATE_RESTAURANT,JSON.stringify(restaurante),{headers:new HttpHeaders({
        'content-type':'application/json'
      })}).toPromise();
    }
  
      /**
     * Modificar restauranteo
     * @param restaurante 
     */
       updaterestaurante(restaurante:any,id: any){
        return this.http.put(`${API_PRODS.UPDATE_RESTAURANTS}?id=${id}`,JSON.stringify(restaurante),{headers:new HttpHeaders({
          'content-type':'application/json'
        })}).toPromise();
      }
  
    /**
     * Eliminar restauranteo
     * @param id 
     */
    eliminarrestaurante(id:number){
      return this.http.delete(`${API_PRODS.DELETE_RESTAURANT}?id=${id}`).toPromise();
    }

}
