import { TestBed } from '@angular/core/testing';

import { RestAuthGuard } from './rest-auth.guard';

describe('RestAuthGuard', () => {
  let guard: RestAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RestAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
