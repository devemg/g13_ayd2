import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersAuthGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    if(sesion){
      let usuariologeado = sesion[0];
      if(usuariologeado){
        if(usuariologeado.rol_id == 1){
          return true;
        }
      }
    }
    this.router.navigateByUrl('');
    return false;
  }
  
}
