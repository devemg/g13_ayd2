import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_PRODS } from "../../environments/environment";

export interface Product{
  nombre?:string;
  precio?:number;
  descripcion?:string;
  id?:number;
  fotografia?:string;
  oferta?: number;
  restaurante: string | number;
}

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  API = "http://localhost:8080/create-product";

  constructor(private http:HttpClient) { }


  uploadImage(image:any){
    return this.http.post(API_PRODS.UPLOAD_IMAGES, image);
  }


  /**
   * Obtener Productos
   */
  getProducts():Promise<Product[]>{
    return this.http.get<Product[]>(API_PRODS.GET_PRODUCTS).toPromise();
  }

  /**
   * Obtener Producto
   * @param id 
   */
  getProduct(id:number):Promise<Product[]>{
    return this.http.get<Product[]>(`${API_PRODS.SINGLE_PRODUCT}?id=${id}`).toPromise();
  }

  /**
   * Agregar producto
   * @param product 
   */
  addProduct(product:Product){
    return this.http.post(API_PRODS.CREATE_PRODUCT,JSON.stringify(product),{headers:new HttpHeaders({
      'content-type':'application/json'
    })}).toPromise();
  }

    /**
   * Modificar producto
   * @param product 
   */
     updateProduct(product:Product,id: any){
      return this.http.put(`${API_PRODS.UPDATE_PRODUCT}?id=${id}`,JSON.stringify(product),{headers:new HttpHeaders({
        'content-type':'application/json'
      })}).toPromise();
    }

  /**
   * Eliminar Producto
   * @param id 
   */
  eliminarProducto(id:number){
    return this.http.delete(`${API_PRODS.DELETE_PRODUCT}?id=${id}`).toPromise();
  }

  getOffers(){
    return this.http.get(`${API_PRODS.OFFER_PRODUCT}`).toPromise();
  }

  getAllRest(){
    return this.http.get(`${API_PRODS.GET_RESTAURANTS}`).toPromise();
  }

  /**
   * Obtener Productos
   */
   getDummyProducts():Product[]{
    return [];
  }
  
}