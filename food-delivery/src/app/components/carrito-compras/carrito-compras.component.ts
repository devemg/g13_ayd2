import { Component, DoCheck, OnInit } from '@angular/core';
import { CarritoComprasService, Producto } from 'src/app/services/carrito-compras.service';

@Component({
  selector: 'carrito-compras',
  templateUrl: './carrito-compras.component.html',
  styleUrls: ['./carrito-compras.component.css']
})
export class CarritoComprasComponent implements OnInit,DoCheck {

  productos:Producto[] = [];
  total:number = 0; 

  constructor(private carrito:CarritoComprasService){
    
  }

  load(){
    this.productos = this.carrito.getProductos();
    this.total = this.carrito.getTotal();
  }

  deleteProduct(id:number){
    this.carrito.deleteProducto(id)
    this.load()
  }
  ngOnInit(): void {
    this.load()
  }

  ngDoCheck(){
    this.load()
  }

}
