import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-menu-restaurante',
  templateUrl: './menu-restaurante.component.html',
  styleUrls: ['./menu-restaurante.component.css']
})
export class MenuRestauranteComponent implements OnInit {
  destroy$: Subject<void>;

  user :any = null;
  constructor(private router: Router, private auth: LoginService) { 
    this.destroy$ = new Subject();
    this.checkUser(localStorage.getItem('usuarioactual') != null)
  }

  ngOnInit(): void {
    this.auth.isLoggedVar().pipe(takeUntil(this.destroy$)).subscribe(res=>{
        this.checkUser(res);
    })
  }

  checkUser(res:boolean){
    if(res){
      let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    let usuariologeado = sesion[0];
    console.log(usuariologeado)
    if(usuariologeado){
      if(usuariologeado.id != 'anonimo'){
        this.user = {        
          nombre: usuariologeado.nombre
        };
      }
    }
    }else {
      this.user = null;
    }
  }

  ngOnDestroy(){
    this.destroy$.next();
    this.destroy$.complete();
  }

  logout(){
    localStorage.removeItem('usuarioactual');
    this.router.navigateByUrl('/');
    this.auth.setIsLogged(false);
  }


}
