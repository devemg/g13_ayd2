import { Component, HostBinding, OnInit } from '@angular/core';
import { CarritoComprasService } from 'src/app/services/carrito-compras.service';
import { Catalogo, Producto } from 'src/app/models/productos.interface';
import { CatalogoProductoService } from '../../services/producto/catalogo-producto.service';

@Component({
  selector: 'app-catalogo-producto',
  templateUrl: './catalogo-producto.component.html',
  styleUrls: ['./catalogo-producto.component.css']
})
export class CatalogoProductoComponent implements OnInit {

  

  public catalogo: Catalogo;

  public producto: Producto;
  

  public nombre:string = '';

  public spinner:boolean = false;

  constructor(private catalogoProductoService: CatalogoProductoService, private carritoService:CarritoComprasService) {
    this.catalogo = {};
    this.producto = {};
   }

  ngOnInit(): void {
    this.get_productos();
    this.getAUTH();
    //this.catalogo = this.catalogoProductoService.getDummyProds();
  }

  get_productos():void{
    this.catalogoProductoService.get_productos().subscribe(
      res =>{
        this.catalogo = res;
        console.log(res);
      },
      err =>{
        console.log(err);
      }
    )
  }

  detalle_producto(parametro: Producto ):void{
   this.producto = parametro;
  }

  imprimir():void{
    console.log(this.nombre);
  }

  search_producto():void{
    this.spinner = true;
    if(this.nombre != ''){
      this.catalogoProductoService.search_api(this.nombre).subscribe(
        res =>{
          this.catalogo = res;
          this.spinner = false;
          this.nombre = '';
        }, 
        err => {
          console.log(err);
          this.spinner = false;
          this.nombre = '';
        }
      )
    }else{
      //TODO: mensaje para escriba algo para la busquedad
      this.get_productos();
      this.spinner = false;
    }
  }

  /*****************************************
   * Metodo de Nery
  */

  usuariologeado: any;

  public getAUTH()
  {
    let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    this.usuariologeado = sesion[0];
    console.log(sesion)
    /*
    Ejemplo de estructutra resultante del this.usuariologeado:
      contrasenia: "123"
      correo: "nh@gmail.com"
      direccion: "15 ave 15-15 zona 15"
      id: 1 
      nombre: "Nery Herrera"
      rol_id: 2 <-- este es el rol
      telefono: "55448855"
    */
    //console.log(this.usuariologeado.correo)
  }

  // -----------------------------------------------------------------------------

  addtoCart(){
    if(this.producto){
      if(this.producto.nombre && this.producto.id && this.producto.fotografia && this.producto.precio){
        this.carritoService.addProducto({
          nombre:this.producto.nombre,
          cantidad:1,
          id:this.producto.id,
          imagen:this.producto.fotografia,
          precio:this.producto.precio
        });
      }
    }
  }
}
