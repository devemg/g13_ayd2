import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductsService } from 'src/app/services/products.service';
import { RestauranteService } from 'src/app/services/restaurante.service';
import Swal from 'sweetalert2';
import { ImgSnippet } from '../../products/create-product/create-product.component';


@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {

  nombre:FormControl;
  direccion:FormControl;
  imagen:ImgSnippet;

  constructor(private prodService:ProductsService, private restService:RestauranteService, private router:Router) { 
    this.nombre = new FormControl('',[Validators.required])
    this.direccion = new FormControl('',[Validators.required])
    this.imagen = {
      base64:'',
      file:''
    }
  }

  ngOnInit(): void {
  }

  /**
   * Guardar Producto
   */
  save(){
    //guardar imagen 
    const formData = new FormData();
    formData.append('image', this.imagen.file);    
    console.log('guardando...');
    this.prodService.uploadImage(formData).subscribe(
      (res:any) => {
        console.log(res)
        this.saveProduct(res.url);
      },
      (err) => {
        console.log(err);
        this.saveProduct('');
      }
    );
  }
  

  saveProduct(url:string){
    this.restService.addrestaurante({
      direccion:this.direccion.value,
      nombre:this.nombre.value,
      fotografia:url,
    }).then(res=>{
      console.log(res);
      Swal.fire(
        'Nuevo Restaurante',
        'Guardado con éxito',
        'success'
      ).then(re=>{
        this.router.navigateByUrl('/restaurantes');
      })
    })
    .catch(er=>{
      console.log(er);
      
      Swal.fire(
        'Nuevo Restaurante',
        'No se pudo guardar el restaurante',
        'error'
      )
    })
  }

    /**
   * Mostrar el preview de imagenes
   * @param files 
   */
  onFileChange(event:any){
    
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.imagen.file = file;

      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
       // this.message = "Only images are supported.";
        return;
      }  
      var reader = new FileReader();
      reader.readAsDataURL(file); 
      reader.onload = (_event) => { 
        this.imagen.base64 = reader.result; 
      }
    }
    }
  }