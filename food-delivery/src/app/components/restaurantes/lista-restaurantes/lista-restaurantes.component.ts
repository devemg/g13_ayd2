import { Component, OnInit } from '@angular/core';
import { Product, ProductsService } from 'src/app/services/products.service';
import { RestauranteService } from 'src/app/services/restaurante.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lista-restaurantes',
  templateUrl: './lista-restaurantes.component.html',
  styleUrls: ['./lista-restaurantes.component.css']
})
export class ListaRestaurantesComponent implements OnInit {

  productos:any[];

  constructor(private resService:RestauranteService) { 
    this.productos = [];
    this.load()
  }

  ngOnInit(): void {
   // this.Cargar_Productos_Prueba();
  }


  /**
   * Cargar productos
   */
  async load(){
    this.resService.getrestaurantes()
    .then((res:any)=>{
      console.log(res)
      this.productos = res.msg;
    })
    .catch(e=>console.log(e));
  }

  /**
   * Eliminar Producto
   * @param id 
   */
  delete(id?:number){
    console.log('eliminar ',id)
    if(id){
      this.resService.eliminarrestaurante(id).then(res=>{
        console.log(res)
        Swal.fire(
          'Restaurante eliminado',
          'Restaurante eliminado con éxito',
          'success'
        ).then(e=>{
          this.load();
        })
      },error=>{
        console.log(error)
        Swal.fire(
          'Error',
          'Restaurante no eliminado',
          'error'
        )
      })
    }
  }
}
