import { Component, OnInit } from '@angular/core';
import { CarritoComprasService } from 'src/app/services/carrito-compras.service';
import { Product, ProductsService } from 'src/app/services/products.service';
import { Restaurante } from '../../models/restaurant.model';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  restaurantes: Restaurante[] = [];
  productosOferta: Product[] = [];
  constructor(private products: ProductsService,private carritoService:CarritoComprasService) { }

  ngOnInit(): void {
    this.fill();
    this.productos();
    this.slides = this.chunk(this.productosOferta, 3);
    console.log(environment);
  }

  slides: any = [[]];
  chunk(arr: any, chunkSize:any) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }


  async fill(){
    // limitar a seis
    
    let res:any = await this.products.getAllRest();
     let list = res.msg; 
     let i;
     for(i = 0; i<list.length&&i<6;i++ ){
      this.restaurantes.push({
        id:list[i].id,
        logo: list[i].fotografia,
        nombre:list[i].nombre
      });
     }
  }

  async productos(){
    //this.productosOferta = this.products.getDummyProducts();
    let prods:any = await this.products.getOffers();
    console.log(prods);
    let listaLarga = prods.msg;
    let i = 0;
    let j = 0;
    let slide: any[] = [];
   if(listaLarga){
    for(j = 0; j<listaLarga.length; j++){
      if(i<3){
        slide.push(listaLarga[j]);
        i++;
      }else{
        this.slides.push(slide);
        slide = new Array;
        slide.push(listaLarga[j]);
        i = 0;
      }
    }
    if(slide.length > 0){
      this.slides.push(slide);
    }
    console.log(this.slides.length)
   }
  }

  addtoCart(producto: any){
        this.carritoService.addProducto({
          nombre:producto.nombre,
          cantidad:1,
          id:producto.id,
          imagen:producto.fotografia,
          precio:producto.oferta != 0? producto.oferta:producto.precio
        });
        Swal.fire(
          'Orden',
          'Producto Agregado',
          'success'
        )
      }
}
