import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersClienteComponent } from './orders-cliente.component';

describe('OrdersClienteComponent', () => {
  let component: OrdersClienteComponent;
  let fixture: ComponentFixture<OrdersClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
