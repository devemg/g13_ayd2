import { Component, OnInit } from '@angular/core';

import {Orden_cliente, Res_Orden} from '../../../models/orden.interface';
import {OrdersService} from '../../../services/orders.service';
@Component({
  selector: 'app-orders-cliente',
  templateUrl: './orders-cliente.component.html',
  styleUrls: ['./orders-cliente.component.css']
})
export class OrdersClienteComponent implements OnInit {

  public lista_ordenes: Res_Orden;
  public id_cliente:number;

  constructor(private ordersServices: OrdersService) { 
    this.lista_ordenes = {};
    this.id_cliente = -1;
  }

  ngOnInit(): void {
    this.getAUTH();
    this.id_cliente = this.usuariologeado.id;
    //this.id_cliente = 1;
    this.get_orden_cliente();
  }


  //TODO: probar este metodo
  get_orden_cliente():void{
    this.ordersServices.get_orden_cliente(this.id_cliente).subscribe(
      res =>{
        this.lista_ordenes = res;
      },
      err =>{
        console.log(err);
      }
    )
  }

  usuariologeado: any;

  public getAUTH()
  {
    let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    this.usuariologeado = sesion[0];
    //console.log(sesion)
    /*
    Ejemplo de estructutra resultante del this.usuariologeado:
      contrasenia: "123"
      correo: "nh@gmail.com"
      direccion: "15 ave 15-15 zona 15"
      id: 1 
      nombre: "Nery Herrera"
      rol_id: 2 <-- este es el rol
      telefono: "55448855"
    */
    //console.log(this.usuariologeado.correo)
  }



}
