import { Component, OnInit } from '@angular/core';

import {Estado_Orden, Orden_detalle, Res_Estado} from '../../../models/orden.interface';
import {OrdersService} from '../../../services/orders.service';
import Swal from'sweetalert2';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})
export class ListOrdersComponent implements OnInit {


  public lista_orden: Array<Orden_detalle>;

  public lista_estados: Res_Estado;

  public valor?:number;
  public Orden:Orden_detalle;

  constructor(private ordersServices: OrdersService) { 
    this.lista_orden = [];
    this.lista_estados = {};
    this.valor = -1;
    this.Orden = {};
  }

  ngOnInit(): void {
    this.Cargar_Ordenes_Prueba();
    this.Cargar_Estado_Prueba();
  }

  get_orders():void{
    this.ordersServices.get_orden().subscribe(
      res =>{
        this.lista_orden = res;
      }, 
      err =>{
        console.log(err);
      }
    )
  }

  get_estado():void{
    this.ordersServices.get_estado().subscribe(
      res =>{
        this.lista_estados = res;
      },
      err =>{
        console.log(err);
      }
    )
  }

  capturar_orden(Orden: Orden_detalle):void{
    this.Orden = Orden;
    this.valor = Orden.estado_orden;
  }

  update_orden():void{
    if(this.valor != -1){

      var datos = {
        id_estado: this.valor,
        id_orden: this.Orden.id
      }
      
      this.ordersServices.update_orden(datos).subscribe(
        res =>{
          console.log(res);
          this.valor = -1
          this.Orden = {};
          Swal.fire(
            'Estado de Orden',
            'Cambiado con exito',
            'success'
          )
        },
        err =>{
          console.log(err);
          this.valor = -1;
          this.Orden = {};
          Swal.fire(
            'Estado de Orden',
            'No se puede realizar el cambio',
            'error'
          )
        }
      )

    }
  }


  Cargar_Ordenes_Prueba():void{
    this.lista_orden = [
      {id: 1, fecha_hora: new Date(), total: 100, nombre_cliente:'Pablito', direccion:'Usac', estado_orden: 1, nombre_estado:'Pendiente'},
      {id: 2, fecha_hora: new Date(), total: 100, nombre_cliente:'Pablito', direccion:'Usac', estado_orden: 1, nombre_estado:'Pendiente'},
      {id: 3, fecha_hora: new Date(), total: 100, nombre_cliente:'Pablito', direccion:'Usac', estado_orden: 1, nombre_estado:'Pendiente'},
      {id: 4, fecha_hora: new Date(), total: 100, nombre_cliente:'Pablito', direccion:'Usac', estado_orden: 1, nombre_estado:'Pendiente'},
      {id: 5, fecha_hora: new Date(), total: 100, nombre_cliente:'Pablito', direccion:'Usac', estado_orden: 1, nombre_estado:'Pendiente'},
      {id: 6, fecha_hora: new Date(), total: 100, nombre_cliente:'Pablito', direccion:'Usac', estado_orden: 1, nombre_estado:'Pendiente'}
    ]
  }

  Cargar_Estado_Prueba():void{
    this.lista_estados = {
      status: 200,
      datos:[
        {id: 1, nombre: "Pendiente"},
        {id: 2, nombre: "Proceso"},
        {id: 3, nombre: "Enviado"},
        {id: 4, nombre: "Entregado"}
      ]
    }
  }

}
