import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  user: any = {
    email: '',
    password: '',
  };

  message:string = '';

  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  iniciarSesion()
  {
    this.loginService.loginUser(this.user).subscribe(
      res => {
        console.log(res);
        const respuesta:any = res;
        if(respuesta.length == 0)
        {
          this.message = "Datos incorrectos";
          //alert('Datos de ingreso incorrectos');
        }
        else
        {
          //alert('Bienvenido');
          localStorage.setItem("usuarioactual",JSON.stringify(respuesta))
          console.log(JSON.stringify(respuesta))
          this.router.navigate(['/catalogo']);
          this.loginService.setIsLogged(true);
        }
      }
    )
  }

  iniciarSesion_anonimamente()
  {
    let anonimo:any = [{
      contrasenia: "anonimo", 
      correo: "anonimo", 
      direccion: "anonimo", 
      id: "anonimo",
      nombre: "anonimo",
      rol_id: 2,
      telefono: "anonimo"
    }]
    alert("Ingreso como anonimo")
    localStorage.setItem("usuarioactual",JSON.stringify(anonimo))
    console.log(JSON.stringify(anonimo))
    this.router.navigate(['/catalogo']);
    this.loginService.setIsLogged(true);
  }

}
