import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';
import { ImgSnippet } from '../create-product/create-product.component';
import Swal from'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

 
  nombre:FormControl;
  precio:FormControl;
  oferta:FormControl;
  descripcion:FormControl;
  imagen:ImgSnippet;
  idProducto = 0;
  constructor(private prodService:ProductsService,private activateRouter:ActivatedRoute,private router:Router) { 
    this.nombre = new FormControl('',[Validators.required])
    this.precio = new FormControl(0,[Validators.required])
    this.oferta = new FormControl(0,[Validators.required])
    this.descripcion = new FormControl('',[Validators.required])
    this.imagen = {
      base64:'',
      file:''
    }
    this.activateRouter.params.subscribe(res=>{
      this.load(res.id)
      this.idProducto = res.id;
    })
  }

  ngOnInit(): void {
  }

  /**
   * Guardar Producto
   */
  save(){
    //guardar imagen 
    //guardar imagen 
    if(!this.imagen.file){
      this.saveProduct(this.imagen.base64);
      return;
    }
    const formData = new FormData();
    formData.append('image', this.imagen.file);    
    console.log('guardando...');
    this.prodService.uploadImage(formData).subscribe(
      (res:any) => {
        console.log(res)
        this.saveProduct(res.url);
      },
      (err) => {
        console.log(err);
        this.saveProduct('');
      }
    );
  }

  saveProduct(url:any){
    let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    let usuariologeado = sesion[0];

    this.prodService.updateProduct({
      descripcion:this.descripcion.value,
      nombre:this.nombre.value,
      precio:this.precio.value,
      fotografia:url,
      restaurante: usuariologeado.rol_id,
      oferta: this.oferta.value
    },this.idProducto).then(res=>{
      Swal.fire(
        'Nuevo Producto',
        'Guardado con éxito',
        'success'
      ).then(re=>{
        this.router.navigateByUrl('/productos');
      })
    })
    .catch(er=>{
      Swal.fire(
        'Nuevo Producto',
        'No se pudo guardar el producto',
        'error'
      )
    })
  }

    /**
   * Mostrar el preview de imagenes
   * @param files 
   */
  onFileChange(files:any){
    if (files.length === 0)
        return;
   
      var mimeType = files[0].type;
      if (mimeType.match(/image\/*/) == null) {
       // this.message = "Only images are supported.";
        return;
      }
      this.imagen.file =  files[0];
      //this.extension = files[0].type.split('/').pop();
  
      var reader = new FileReader();
      reader.readAsDataURL(files[0]); 
      reader.onload = (_event) => { 
        this.imagen.base64 = reader.result; 
      }
    }

    load(id:number){
      this.prodService.getProduct(id)
      .then((res:any)=>res.msg)
      .then(res=>{
        if(res.length>0){
          this.nombre.setValue(res[0].nombre)
          this.descripcion.setValue(res[0].nombre)
          this.precio.setValue(res[0].precio)
          this.imagen.base64 = res[0].fotografia
          this.oferta.setValue(res[0].oferta)
        }
        
      })
    }
}
