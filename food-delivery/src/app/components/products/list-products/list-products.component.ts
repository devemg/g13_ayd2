import { Component, OnInit } from '@angular/core';
import { Product, ProductsService } from 'src/app/services/products.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {

  productos:Product[];

  constructor(private productService:ProductsService) { 
    this.productos = [];
    this.load()
  }

  ngOnInit(): void {
   // this.Cargar_Productos_Prueba();
  }


  /**
   * Cargar productos
   */
  async load(){
    this.productService.getProducts()
    .then((res:any)=>{
      console.log(res)
      this.productos = res.msg;
    })
    .catch(e=>console.log(e));
  }

  /**
   * Eliminar Producto
   * @param id 
   */
  delete(id?:number){
    if(id){
      this.productService.eliminarProducto(id).then(res=>{
        console.log(res)
        Swal.fire(
          'Producto eliminado',
          'Producto eliminado con éxito',
          'success'
        ).then(e=>{
          this.load();
        })
      },error=>{
        console.log(error)
        Swal.fire(
          'Error',
          'Producto no eliminado',
          'error'
        )
      })
    }
  }

  Cargar_Productos_Prueba():void{
    this.productos = [
     /*/ {nombre:'Papas fritas',descripcion:"deliciosas y suculetas", precio: 20,id:1},
      {nombre:'Papas fritas',descripcion:"deliciosas y suculetas", precio: 20,id:2},
      {nombre:'Papas fritas',descripcion:"deliciosas y suculetas", precio: 20,id:3},
      {nombre:'Papas fritas',descripcion:"deliciosas y suculetas", precio: 20,id:4}*/
    ]
  }

}
