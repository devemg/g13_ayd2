import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product, ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit {

  producto:Product;
  constructor(private prodService:ProductsService,private activateRouter:ActivatedRoute) {
    this.producto = {
      descripcion:'',
      nombre:'',
      precio:0,
      fotografia:'',
      id:1,
      restaurante:1
    }
    this.activateRouter.params.subscribe(res=>{
      this.load(res.id)
    })
   }

  ngOnInit(): void {
  }

  load(id:number){
    this.prodService.getProduct(id)
    .then((res:any)=>res.msg)
    .then(res=>{
      
      console.log(res)
      if(res.length > 0){
        this.producto = res[0]
      }
    })
  }

}
