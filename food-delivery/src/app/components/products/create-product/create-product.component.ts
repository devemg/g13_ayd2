import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductsService } from 'src/app/services/products.service';
import Swal from 'sweetalert2';



export interface ImgSnippet{
  base64:string|ArrayBuffer|null; 
  file:string|Blob;
}

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  nombre:FormControl;
  precio:FormControl;
  descripcion:FormControl;
  imagen:ImgSnippet;
  oferta:FormControl;

  mensaje:string;

  constructor(private prodService:ProductsService, private router:Router) { 
    this.nombre = new FormControl('',[Validators.required])
    this.precio = new FormControl(0,[Validators.required])
    this.oferta = new FormControl(0,[])
    this.descripcion = new FormControl('',[Validators.required])
    this.imagen = {
      base64:'',
      file:''
    }
    this.mensaje = "";
  }

  ngOnInit(): void {
  }

  /**
   * Guardar Producto
   */
  save(){
    //guardar imagen 
    const formData = new FormData();
    formData.append('image', this.imagen.file);    
    console.log('guardando...');
    this.prodService.uploadImage(formData).subscribe(
      (res:any) => {
        console.log(res)
        this.saveProduct(res.url);
      },
      (err) => {
        console.log(err);
        this.saveProduct('');
      }
    );
  }
  

  saveProduct(url:string){
    let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
    let usuariologeado = sesion[0];

    this.prodService.addProduct({
      descripcion:this.descripcion.value,
      nombre:this.nombre.value,
      precio:this.precio.value,
      fotografia:url,
      restaurante: usuariologeado.rol_id, 
      oferta: this.oferta.value
    }).then(res=>{
      console.log(res);
      Swal.fire(
        'Nuevo Producto',
        'Guardado con éxito',
        'success'
      ).then(re=>{
        this.router.navigateByUrl('/productos');
      })
    })
    .catch(er=>{
      console.log(er);
      this.mensaje = "producto no guardado"
      Swal.fire(
        'Nuevo Producto',
        'No se pudo guardar el producto',
        'error'
      )
    })
  }

    /**
   * Mostrar el preview de imagenes
   * @param files 
   */
  onFileChange(event:any){
    
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.imagen.file = file;

      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
       // this.message = "Only images are supported.";
        return;
      }  
      var reader = new FileReader();
      reader.readAsDataURL(file); 
      reader.onload = (_event) => { 
        this.imagen.base64 = reader.result; 
      }
    }
    }

}
