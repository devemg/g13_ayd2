import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RootConsoleMysqlComponent } from './root-console-mysql.component';

describe('RootConsoleMysqlComponent', () => {
  let component: RootConsoleMysqlComponent;
  let fixture: ComponentFixture<RootConsoleMysqlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RootConsoleMysqlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RootConsoleMysqlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
