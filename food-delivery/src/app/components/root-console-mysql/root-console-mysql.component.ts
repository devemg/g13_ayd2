import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-root-console-mysql',
  templateUrl: './root-console-mysql.component.html',
  styleUrls: ['./root-console-mysql.component.css']
})
export class RootConsoleMysqlComponent implements OnInit {

  user: any = {
    consulta: ''
  };

  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  consultar()
  {
    console.log(this.user);
    this.loginService.root_mysql(this.user).subscribe(
      res => {
        console.log(res)
      }
    )
  }

}
