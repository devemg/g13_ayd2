import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from 'src/app/services/products.service';
import { RestauranteService } from 'src/app/services/restaurante.service';
import { LoginService } from '../../services/login.service';
import { ImgSnippet } from '../products/create-product/create-product.component';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  user: any = {
    nombre : '',
    correo : '',
    direccion :'',
    telefono :'',
    contrasenia :'',
    rol_id :1
  };

  imagen:ImgSnippet;

  constructor(private router: Router, private authservice: LoginService, private activatedRouter: ActivatedRoute,
    private prodService: ProductsService, private restService: RestauranteService) {
    const param = this.activatedRouter.snapshot.params;
    console.log(param);
    this.user.rol_id = param.algo?2:1;
    this.imagen = {
      base64:'',
      file:''
    }
   }

  ngOnInit(): void {
  }

  registrar()
  {
    if(this.user.rol_id == 2 ){
      this.crearRestaurante();
    }else {
      this.authservice.registrarUser(this.user).subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/iniciar-sesion']);
        }, error =>{
          console.log(error)
        }
      )
    }
    
  }

    /**
   * Guardar Producto
   */
     async crearRestaurante(){
      //guardar imagen 
      const formData = new FormData();
      formData.append('image', this.imagen.file);    
      console.log('guardando...');
      this.prodService.uploadImage(formData).subscribe(
        (res:any) => {
          console.log(res)
          this.user.fotografia = res.url;
          this.authservice.registrarUser(this.user).subscribe(
            async res => {
              console.log(res);
              await this.restService.addrestaurante(
                {nombre:this.user.nombre,fotografia:this.user.fotografia,direccion:this.user.direccion}
                );
              this.router.navigate(['/iniciar-sesion']);
            }, error =>{
              console.log(error)
            }
          )
        },
        (err) => {
          console.log(err);
          this.authservice.registrarUser(this.user).subscribe(
            async res => {
              console.log(res);
              await this.restService.addrestaurante(
                {nombre:this.user.nombre,fotografia:this.user.fotografia,direccion:this.user.direccion}
                );
              this.router.navigate(['/iniciar-sesion']);
            }, error =>{
              console.log(error)
            }
          )
        }
      );
    }

      /**
   * Mostrar el preview de imagenes
   * @param files 
   */
       onFileChange(event:any){
    
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.imagen.file = file;
    
          var mimeType = file.type;
          if (mimeType.match(/image\/*/) == null) {
           // this.message = "Only images are supported.";
            return;
          }  
          var reader = new FileReader();
          reader.readAsDataURL(file); 
          reader.onload = (_event) => { 
            this.imagen.base64 = reader.result; 
          }
        }
        }
    

}
