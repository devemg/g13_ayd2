import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {CatalogoProductoService} from './services/producto/catalogo-producto.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateProductComponent } from './components/products/create-product/create-product.component';
import { ListProductsComponent } from './components/products/list-products/list-products.component';
import { EditProductComponent } from './components/products/edit-product/edit-product.component';
import { SingleProductComponent } from './components/products/single-product/single-product.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { CatalogoProductoComponent } from './components/catalogo-producto/catalogo-producto.component';
import { RootConsoleMysqlComponent } from './components/root-console-mysql/root-console-mysql.component';
import { ListOrdersComponent } from './components/order/list-orders/list-orders.component';
import { OrdersClienteComponent } from './components/order/orders-cliente/orders-cliente.component';
import { CarritoComprasComponent } from './components/carrito-compras/carrito-compras.component';
import { HomeComponent } from './components/home/home.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { MenuRestauranteComponent } from './components/menu-restaurante/menu-restaurante.component';
import { MenuClienteComponent } from './components/menu-cliente/menu-cliente.component';
import { ListaRestaurantesComponent } from './components/restaurantes/lista-restaurantes/lista-restaurantes.component';
import { CrearComponent } from './components/restaurantes/crear/crear.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateProductComponent,
    ListProductsComponent,
    EditProductComponent,
    SingleProductComponent,
    MenuComponent,
    CatalogoProductoComponent,
    LoginComponent,
    RegistroComponent,
    RootConsoleMysqlComponent,
    ListOrdersComponent,
    OrdersClienteComponent,
    CarritoComprasComponent,
    HomeComponent,
    MenuRestauranteComponent,
    MenuClienteComponent,
    ListaRestaurantesComponent,
    CrearComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [
    CatalogoProductoService  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
