import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateProductComponent } from './components/products/create-product/create-product.component';
import { EditProductComponent } from './components/products/edit-product/edit-product.component';
import { ListProductsComponent } from './components/products/list-products/list-products.component';
import { SingleProductComponent } from './components/products/single-product/single-product.component';
import { CatalogoProductoComponent } from './components/catalogo-producto/catalogo-producto.component';
import { RootConsoleMysqlComponent } from './components/root-console-mysql/root-console-mysql.component';
import { ListOrdersComponent } from './components/order/list-orders/list-orders.component';
import { OrdersClienteComponent } from './components/order/orders-cliente/orders-cliente.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ListaRestaurantesComponent } from './components/restaurantes/lista-restaurantes/lista-restaurantes.component';
import { CrearComponent } from './components/restaurantes/crear/crear.component';
import { RestAuthGuard } from './services/rest-auth.guard';
import { UsersAuthGuard } from './services/users-auth.guard';
const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'iniciar-sesion',
    component:LoginComponent
  },
  {
    path:'registro/:algo',
    component:RegistroComponent
  },
  {
    path:'registro',
    component:RegistroComponent
  },
  {
    path: 'catalogo', 
    component: CatalogoProductoComponent
  },
  {
    path: 'ordenes', 
    component: ListOrdersComponent,
    //canActivate: [ RestAuthGuard ]
  },
  {
    path: 'productos',
    component: ListProductsComponent,
    //canActivate: [ RestAuthGuard ]
  },
  {
    path:'detalle-producto/:id',
    component:SingleProductComponent,
    //canActivate: [ RestAuthGuard ]
  },
  {
    path:'productos/editar/:id',
    component:EditProductComponent,
    //canActivate: [ RestAuthGuard ]
  },
  {
    path:'productos/nuevo',
    component:CreateProductComponent,
    //canActivate: [ RestAuthGuard ]
  },
  {
    path: 'ordenes-cliente',
    component: OrdersClienteComponent,
    //canActivate: [ UsersAuthGuard ]
  },
  {
    path: 'restaurantes',
    component: ListaRestaurantesComponent
  },
  {
    path: 'restaurantes/crear',
    component: CrearComponent
  }
  /*,
  ,
  {path: 'login', component: LoginComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'root', component: RootConsoleMysqlComponent},
  {path: 'orders', component: ListOrdersComponent},
  {path: 'orders-cliente', component: OrdersClienteComponent}*/
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
