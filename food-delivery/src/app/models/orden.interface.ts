export interface Orden_detalle{
    id?:number;
    fecha_hora?: Date;
    total?: number;
    nombre_cliente?: string;
    direccion?: string;
    estado_orden?:number;
    nombre_estado?: string;
}

export interface Orden_cliente{
    fecha_hora?:Date;
    total?:number;
    direccion?:string;
    nombre_estado?:string;
}

export interface Estado_Orden{
    id?: number;
    nombre?: string;
}

export interface Res_Estado{
    datos?: Array<Estado_Orden>;
    status?:number
}

export interface Res_Orden{
    datos?: Array<Orden_cliente>;
    status?:number;
}