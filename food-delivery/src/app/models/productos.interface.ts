
export interface Producto{
    id?: number;
    nombre?: string;
    fotografia?:string;
    precio?: number;
    descripcion?: string;
    oferta? : number;
    restaurante?: string;
    logo_restaurante?: string;
    nombre_restaurante?: string;
}

export interface Catalogo{
    msg?:Array<Producto>
    status?:number;
};
