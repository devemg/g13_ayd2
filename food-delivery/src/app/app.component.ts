import { Component } from '@angular/core';
import { CarritoComprasService } from './services/carrito-compras.service';
import { OrdersService } from './services/orders.service'
import {HttpClient} from '@angular/common/http';
import { Catalogo, Producto } from 'src/app/models/productos.interface';
import { FocusTrap } from '@angular/cdk/a11y';
import { Chain } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'food-delivery';

  constructor(private carritoService:CarritoComprasService,
              private ordenesService:OrdersService,
              private http:HttpClient){

  }

  vaciarCarrito(){
    this.carritoService.clearCarrito();
  }
  crearOrden(){
    //ESTE ES EL BOTON DE COMPRAR DEL CARRITO, QUE SERÍA GENERAR ORDEN
    let prod = localStorage.getItem('productos');
    if (prod){
      let total  = this.carritoService.getTotal();
      //window.alert("HAY ALGO, Y ESTE ES EL TOTAL---> : "+ total)
      //AQUI CREO LA ORDEN, NECESITO TOTAL,CLIENTEID,USUARIOID,ESTADOID
      let sesion:any = JSON.parse(localStorage.getItem("usuarioactual") || '{}');
      let usuariologeado = sesion[0];//ID DEL CLIENTE
      //console.log(usuariologeado.id)
      let cadenita="";
      let Comillas = "\"";
      if(usuariologeado.id =="anonimo"){
        cadenita = '{"total":'+Comillas+total+Comillas+',"clienteid":"1","usuarioid":"1","estadoid":"1"}'
      }
      else {
        cadenita = '{"total":'+Comillas+total+Comillas+',"clienteid":'+Comillas+usuariologeado.id+Comillas+',"usuarioid":'+Comillas+usuariologeado.id+Comillas+',"estadoid":"1"}'
      }
      
      let id:number;
      let yeison = JSON.parse(prod);
      console.log(cadenita)
      this.ordenesService.crearOrden(JSON.parse(cadenita)).subscribe(
        res =>{
          console.log(res);
          id = res.insertId;
          console.log(id)
          console.log(yeison)
          for(var x of yeison){
            //cantidad, idproducto, idorder
            let chain = '{"cantidad":'+Comillas+x.cantidad+Comillas+',"idproducto":'+Comillas+x.id+Comillas+',"idorder":'+Comillas+id+Comillas+'}'
            this.ordenesService.fillOrder(JSON.parse(chain)).subscribe(
              xd =>{
                console.log(xd)
              }
            );
          }
          this.carritoService.clearCarrito();

        }
      )
      this.carritoService.clearCarrito();
      
      //console.log(yeison);
      for(var x of yeison){
        //console.log(x.id)
      }
      
      
      

      
      
      
    }
    else{
      window.alert("NUAY");
    }
    
  }
}
