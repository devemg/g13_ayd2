/*dependencies*/
var bodyParser = require("body-parser");
/*MYSQL CONNECTION */
const mysql = require('mysql');
/*EXPRES APP */
var express = require("express");
const { response } = require('express');
const cors = require('cors');
const app = express();
app.use(bodyParser.json());
app.use(cors());


app.put("/update/product", (request, response, next) => {
    var id = request.query['id'];
    var { nombre, fotografia, precio, descripcion, oferta, restaurante } = request.body;
    var sql = 'UPDATE producto SET nombre = ?, fotografia = ?, precio = ?, descripcion = ?, oferta = ?, restaurante = ? WHERE id = ?'
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query(sql, [nombre, fotografia, precio, descripcion, oferta, restaurante, id],
            function(err, result) {
                if (err) {
                    console.log(err);
                    return response.json({ 'error': "Error interno", "status": 500 });
                };
                response.json({ 'msg': result, 'status': 200 });
            });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.put("/update/restaurant", (request, response, next) => {
    var id = request.query['id'];
    var { nombre, fotografia, direccion } = request.body;
    var sql = 'UPDATE restaurante SET nombre = ?, fotografia = ?, descripcion = ? WHERE id = ?'
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query(sql, [nombre, fotografia, descripcion],
            function(err, result) {
                if (err) {
                    console.log(err);
                    return response.json({ 'error': "Error interno", "status": 500 });
                };
                response.json({ 'msg': result, 'status': 200 });
            });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});

app.listen(8080, () => {
    console.log("MICROSERVICE: Update running on port 4000");
});
