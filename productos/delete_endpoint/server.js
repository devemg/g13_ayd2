/*dependencies*/
var bodyParser = require("body-parser");
/*MYSQL CONNECTION */
const mysql = require('mysql');
/*EXPRES APP */
var express = require("express");
const { response } = require('express');
const app = express();
const cors = require('cors');
app.use(bodyParser.json());
app.use(cors());


app.delete("/delete/product", (request, response, next) => {
    var id = request.query['id'];
    if (id) {
        var sql = 'DELETE FROM producto WHERE id = ?';
        try {
            let connection = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USER,
                password: process.env.PASSWORD,
                database: process.env.DB,
		    insecureAuth:true
            });

            connection.query(sql, [id], function(err, result) {
                if (err) {
                    return response.json({ 'error': "Error interno", "status": 500 });
                }
                response.json({ 'msg': result, 'status': 200 });
            });
        } catch (error) {
            connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
        }
    }
});


app.delete("/delete/restaurant", (request, response, next) => {
    var id = request.query['id'];
    if (id) {
        var sql = 'DELETE FROM restaurante WHERE id = ?';
        try {
            let connection = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USER,
                password: process.env.PASSWORD,
                database: process.env.DB,
		    insecureAuth:true
            });

            connection.query(sql, [id], function(err, result) {
                if (err) {
                    return response.json({ 'error': "Error interno", "status": 500 });
                }
                response.json({ 'msg': result, 'status': 200 });
            });
        } catch (error) {
            connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
        }
    }
});

app.listen(8080, () => {
    console.log("MICROSERVICE: Delete product running on port 4000");
});
