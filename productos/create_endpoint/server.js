/*dependencies*/
var bodyParser = require("body-parser");
/*MYSQL CONNECTION */
const mysql = require('mysql');
/*EXPRES APP */
var express = require("express");
const { response } = require('express');
const app = express();
const cors = require('cors');
app.use(bodyParser.json());
app.use(cors());

app.post("/create-product", (request, response, next) => {
    var { nombre, fotografia, precio, descripcion, oferta , restaurante} = request.body;

    //Lo agregue para validar el fallo al crear producto :p
    if(nombre === ''){
        return response.status(404).send('Not found');
    }

    var sql = `INSERT INTO producto (nombre, fotografia, precio, descripcion, oferta, restaurante)
                VALUES (?,?,?,?,?,?)`;
    var values = [nombre, fotografia, precio, descripcion, oferta, restaurante];
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
            insecureAuth: true,
        });
        connection.query(sql, values, function(err, result) {
            if (err) {
                console.log(err);
                return response.json({ 'error': "Error interno", "status": 500 });
            }
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});

app.post("/create-restaurant", (request, response, next) => {
    var { nombre, fotografia, direccion } = request.body;

    var sql = `INSERT INTO restaurante (nombre, fotografia, direccion)
                VALUES (?,?,?)`;
    var values = [nombre, fotografia, direccion];
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
            insecureAuth: true,
        });
        connection.query(sql, values, function(err, result) {
            if (err) {
                console.log(err);
                return response.json({ 'error': "Error interno", "status": 500 });
            }
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.post("/log/error", (request, response, next) => {
    var { accion, usuario_id, nivel, origen, extra } = request.body;
    var sql = `INSERT INTO error (accion, usuario_id, nivel, origen, extra)
                VALUES (?,?,?,?)`;
    var values = [accion, usuario_id, nivel, origen, extra];
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB
        });
        connection.query(sql, values, function(err, result) {
            if (err) {
                console.log(err);
                return response.json({ 'error': "Error interno", "status": 500 });
            }
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.listen(8080, () => {
    console.log("MICROSERVICE: Create product running on port 1000");
});