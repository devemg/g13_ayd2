/*MYSQL CONNECTION */
const mysql = require('mysql');
/*EXPRES APP */
var express = require("express");
const { response } = require('express');
const cors = require('cors');
const app = express();
app.use(cors());

app.get("/get-productos", (request, response, next) => {
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query(
            'SELECT p.id, p.nombre, p.fotografia, p.precio, p.descripcion, p.oferta, p.restaurante, r.nombre as nombre_restaurante, r.fotografia as logo_restaurante FROM producto p join restaurante r on r.id = p.restaurante'
            , (error, result) => {
            // if (error) throw error;
            if (error) {
                console.log(error);
                return response.json({ 'error': "Error interno", "status": 500 });
            }
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});

app.get("/get-restaurantes", (request, response, next) => {
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query('select * from restaurante p', (error, result) => {
            // if (error) throw error;
            if (error) {
                console.log(error);
                return response.json({ 'error': "Error interno", "status": 500 });
            }
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.get("/offer/get-productos", (request, response, next) => {
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query('select * from producto p where p.oferta > 0', (error, result) => {
            // if (error) throw error;
            if (error) {
                console.log(error);
                return response.json({ 'error': "Error interno", "status": 500 });
            }
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.get("/get/product", (request, response, next) => {
    var id = request.query['id'];
    var sql = 'SELECT * FROM producto WHERE id = ?';
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query(sql, [id], function(err, result) {
            if (err) {
                return response.json({ 'error': "Error interno", "status": 500 });
            };
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.get("/get/restaurant", (request, response, next) => {
    var id = request.query['id'];
    var sql = 'SELECT * FROM restaurante WHERE id = ?';
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query(sql, [id], function(err, result) {
            if (err) {
                return response.json({ 'error': "Error interno", "status": 500 });
            };
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.get("/get/restaurant/products", (request, response, next) => {
    var id = request.query['id'];
    var sql = 'SELECT * FROM producto p WHERE p.restaurante = ?';
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB,
		insecureAuth:true
        });
        connection.query(sql, [id], function(err, result) {
            if (err) {
                return response.json({ 'error': "Error interno", "status": 500 });
            };
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.get("/search", (request, response, next) => {
    var q = request.query['q'];
    var sql = `SELECT * FROM producto WHERE nombre LIKE '%${q}%' OR descripcion LIKE '%${q}%' `;
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB, 
		insecureAuth:true
        });
        connection.query(sql, function(err, result) {
            if (err) {
                return response.json({ 'error': "Error interno", "status": 500 });
            };
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});


app.get("/search/restaurant", (request, response, next) => {
    var q = request.query['q'];
    var sql = `SELECT * FROM restaurante WHERE nombre LIKE '%${q}%' OR descripcion LIKE '%${q}%' `;
    try {
        let connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DB, 
		insecureAuth:true
        });
        connection.query(sql, function(err, result) {
            if (err) {
                return response.json({ 'error': "Error interno", "status": 500 });
            };
            response.json({ 'msg': result, 'status': 200 });
        });
    } catch (error) {
        connection.end(error => { response.json({ 'msg': error, 'status': 503 }) });
    }
});

app.listen(8080, () => {
    console.log("MICROSERVICE: Read product running on port 1000");
});



//CONNECTION END
