#!/bin/bash
set -eo pipefail
echo "Subiendo Mis imagenes a DockerHub"
filename="version"
while read -r line; do
    echo "$line" 
    # g13_ayd2_auth
    docker tag g13_ayd2_auth registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_auth:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_auth:${line}
    docker tag g13_ayd2_auth registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_auth:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_auth:latest

    #g13_ayd2_create
    docker tag g13_ayd2_create registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_create:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_create:${line}
    docker tag g13_ayd2_create registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_create:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_create:latest

    #g13_ayd2_delete
    docker tag g13_ayd2_delete registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_delete:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_delete:${line}
    docker tag g13_ayd2_delete registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_delete:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_delete:latest

    #g13_ayd2_get
    docker tag g13_ayd2_get registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_get:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_get:${line}
    docker tag g13_ayd2_get registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_get:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_get:latest

    #g13_ayd2_update
    docker tag g13_ayd2_update registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_update:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_update:${line}
    docker tag g13_ayd2_update registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_update:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_update:latest

    # g13_ayd2_imagenes  
    docker tag g13_ayd2_imagenes registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_imagenes:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_imagenes:${line}
    docker tag g13_ayd2_imagenes registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_imagenes:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/g13_ayd2_imagenes:latest

    # angular-prod1 
    docker tag angular-prod1 registry.gitlab.com/devemg/g13_ayd2/angular-prod1:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/angular-prod1:${line}
    docker tag angular-prod1 registry.gitlab.com/devemg/g13_ayd2/angular-prod1:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/angular-prod1:latest

    # angular-prod2 
    docker tag angular-prod2 registry.gitlab.com/devemg/g13_ayd2/angular-prod2:${line}
    docker push registry.gitlab.com/devemg/g13_ayd2/angular-prod2:${line}
    docker tag angular-prod2 registry.gitlab.com/devemg/g13_ayd2/angular-prod2:latest
    docker push registry.gitlab.com/devemg/g13_ayd2/angular-prod2:latest
    
    echo "FIN DEL SCRIPT"
done < "$filename"