var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var expect = chai.expect;
let chaiHttp = require('chai-http');

chai.use(chaiHttp);
const url= process.env.URLHOST || "http://localhost:3000"

describe('Testing de conexion valida con API ', function() {
  describe('Check de arranque', function(){
    it('Valida que la API este levantada con un metodo de prueba get: ', function() {
        chai.request(url)
        .get('/hola')
        .send({})
        .end( function(err,res){
            expect(res).to.have.status(200);
        });
    });
  });
})

/******************************************************************
*   Pruebas de Login
*/
describe('Testing para funciones de Login ', function() {
    describe('Check de retorno de validacion de login', function(){
      it('Validar que el estado de conexion sea 200', function() {
          chai.request(url)
          .post('/login')
          .send({ email: 'email1@gmail.com', password: 'password'})
          .end( function(err,res){
              console.log(res.body)
              expect(res).to.have.status(200);
          });
      });
      it('El email y password enviados contienen un valor valido', function() {
        chai.request(url)
        .post('/login')
        .send({ email: 'email1@gmail.com', password: 'password'})
        .end( function(err,res){
            expect(req.email).not.equal('');
        });
    });
    });
  })

  /******************************************************************
   * Prueba sobre usuarios
   */
  describe('Testing de validacion de usuarios existentes', function() {
    it('Valida que se pueda acceder a los usuarios para verificar que sean existentes', function() {
        chai.request(url)
        .get('/get-alluser')
        .send({})
        .end( function(err,res){
            console.log(res.body);
            expect(res).to.have.status(200);
        });
    });
    it('Valida que se pueda existan usuarios para login', function() {
        chai.request(url)
        .get('/get-alluser')
        .send({})
        .end( function(err,res){
            console.log(res.body);
            expect(res.body).to.not.equal('[]');
        });
    });
  })


  /******************************************************************
*   Pruebas de Login
*/
describe('Testing para funciones de Registro ', function() {
    describe('Check de retorno de validacion de Registro', function(){
      it('Validar que el estado de conexion sea 200', function() {
          chai.request(url)
          .post('/registrar')
          .send({ email: 'email1@gmail.com', password: 'password'})
          .end( function(err,res){
              console.log(res.body)
              expect(res).to.have.status(200);
          });
      });
      it('Los parametros de craecion de cuenta enviados contienen un valor valido', function() {
        chai.request(url)
        .post('/registrar')
        .send({ email: 'email1@gmail.com', password: 'password'})
        .end( function(err,res){
            expect(req.email).not.equal('');
        });
    });
    });
  })





  /** ------------------------------------------------------------------------------
   * PRUEBAS DE FALLO ------------------------------------------------------------------------------
   *  ------------------------------------------------------------------------------
   */ 

   describe('TEST FAIL', function() 
   {
    describe('Check de arranque', function(){
      it('Valida que la API este levantada con un metodo de prueba post no existene: ', function() {
          chai.request(url)
          .post('/hola')
          .send({})
          .end( function(err,res){
              expect(res).to.have.status(404);
          });
      });
    });
  
  
    describe('Prueba datos fail LOGIN', function()
    {  
      it('Validar que el estado de conexion sea 404 al tener una ruta incorrecta', function() 
      {
        chai.request(url)
        .get('/login')
        .send()
        .end( function(err,res){
            expect(res).to.have.status(404);
        });
      });
    });

    describe('Prueba datos fail REGISTRO', function()
    {  
      it('Validar que el estado de conexion sea 404 al tener una ruta incorrecta', function() 
      {
        chai.request(url)
        .get('/registrar')
        .send()
        .end( function(err,res){
            expect(res).to.have.status(404);
        });
      });
    });
  
    describe('Prueba datos fail LOGIN cadena mal articulada', function()
    {  
      it('Enviar una cadena vacia y esperar empty de regreso en el login', function() 
      {
        chai.request(url)
        .post('/login')
        .send()
        .end( function(err,res){
            expect(res).to.be.empty();
        });
      });
    });

    describe('Prueba datos fail Registro cadena mal articulada', function()
    {  
      it('Enviar una cadena vacia al registro y esperar empty de regreso', function() 
      {
        chai.request(url)
        .post('/registrar')
        .send()
        .end( function(err,res){
            expect(res).to.be.empty();
        });
      });
    });

  
  })

  