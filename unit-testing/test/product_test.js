const { describe, it, after, before } = require('mocha');

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
chai.use(chaiHttp);
const BASE_URL =  process.env.URLHOST || "http://localhost:3000"

process.on('unhandledRejection', () => {});

(async function example() {
    try {

        describe('API test Productos', async function() { 
            this.timeout(50000);

            it('API endpoint NOT FOUND, imposible listar todos los productos productos', async() => {                
                const res = await chai
                .request(BASE_URL)
                .get('/get-products');
                expect(res.status).to.equal(404);
                console.log("Codigo de salida" + res.status);
            });


            it('Productos listados correctamente', async() => {                
                const res = await chai
                .request(BASE_URL)
                .get('/get-productos');
                expect(res.status).to.equal(200);
                console.log("Codigo de salida" + res.status);
            });

        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
