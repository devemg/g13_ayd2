const { describe, it, after, before } = require('mocha');
let fs = require('fs');
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
chai.use(chaiHttp);
const BASE_URL = process.env.URLHOST || "http://localhost:8080"

process.on('unhandledRejection', () => {});

(async function example() {
    try {

        describe('API Upload Image', async function() { 
            this.timeout(50000);


            it('API Error al subir imagenes sin archivo', async() => {                
                const res = await chai
                .request(BASE_URL)
                .post('/upload-image');
                expect(res.status).to.equal(500);
                console.log("Codigo de salida" + res.status);
            });

            it('API Subir imagen correctamente', async() => {                
                const res = await chai
                .request(BASE_URL)
                .post('/upload-image')
                .attach('image', fs.readFileSync('./dummy.jpg'), 'dummy.jpg');

                expect(res.status).to.equal(200);
                console.log("Codigo de salida" + res.status);
            });

        });

    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
