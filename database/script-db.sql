create database if not exists  database_deliveryfood;

CREATE TABLE if not exists log (
id int primary key auto_increment,
fecha_hora datetime not null default now(),
tipo_accion varchar(500) not null ,
usuario      varchar(250) not null ,
nivel_accion varchar(100) not null ,
origen  varchar(500) not null ,
informacion text
                 );

CREATE TABLE if not exists error(
id int primary key auto_increment,
fecha_hora datetime not null default now(),
descripcion varchar(500) not null ,
entrada varchar(250) not null ,
mensaje_error varchar(500) not null ,
stacktrace text
);

CREATE TABLE if not exists restaurante(
    id int primary key auto_increment,
    nombre varchar(150) not null ,
    fotografia varchar(200),
    direccion varchar(200)
);

CREATE TABLE if not exists producto(
    id int primary key auto_increment,
    nombre varchar(150) not null ,
    fotografia varchar(200),
    precio double not null  default 0,
    descripcion varchar(200),
    oferta double not null default 0,
    restaurante int not null,
    foreign key (restaurante) references restaurante(id)
);

CREATE TABLE if not exists estado_orden(
    id int primary key auto_increment,
    nombre varchar(150) not null
);

CREATE TABLE if not exists permiso(
    id int primary key auto_increment,
    nombre varchar(150) not null
);

CREATE TABLE if not exists rol(
    id int primary key auto_increment,
    nombre varchar(150) not null
);

CREATE TABLE if not exists rol_permiso(
    id int primary key auto_increment,
    permiso_id int,
    rol_id int,
    foreign key (permiso_id) references permiso(id),
    foreign key (rol_id) references rol(id)
);

CREATE TABLE  if not exists usuario(
    id int primary key auto_increment,
    nombre varchar(200) not null,
    correo varchar(200) not null,
    direccion varchar(200) not null,
    telefono varchar(200) not null,
    contrasenia varchar(200) not null,
    rol_id int,
    foreign key (rol_id) references rol(id)
);

CREATE TABLE if not exists orden(
    id int primary key auto_increment,
    fecha_hora datetime not null default now(),
    total double not null default 0,
    cliente_id int,
    usuario_id int,
    estado_id int,
    foreign key (cliente_id) references usuario(id),
    foreign key (usuario_id) references usuario(id),
    foreign key (estado_id) references estado_orden(id)
);

CREATE TABLE if not exists orden_producto(
    id int primary key auto_increment,
    cantidad int not null default 0,
    producto_id int,
    orden_id int,
    foreign key (producto_id) references producto(id),
    foreign key (orden_id) references orden(id)
);

## Valores por defecto 

insert into rol(nombre) values ('Cliente');
insert into rol(nombre) values ('Administrador');

INSERT INTO estado_orden(nombre) values ('Ingresada');
INSERT INTO estado_orden(nombre) values ('En Preparación');
INSERT INTO estado_orden(nombre) values ('Enviada');
INSERT INTO estado_orden(nombre) values ('Entregada');